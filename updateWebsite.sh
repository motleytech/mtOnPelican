topTime=$(date +%s%N | cut -b1-13)

start=$(date +%s%N | cut -b1-13)
echo 'Zipping up output folder...'
rm -rf motleytechnet/output/*
cd motleytechnet
rm -f archive.zip
pelican content
cd output
zip -q ../archive.zip -r *
cd ..
end=$(date +%s%N | cut -b1-13)
timeTaken=$(bc <<< "scale=2; ($end - $start)/1000")
printf "Time taken : $timeTaken seconds \n\n"


start=$(date +%s%N | cut -b1-13)
echo 'Creating remote temp folder...'
sshpass -f <(printf '%s\n' $DOKEY) ssh root@motleytech.net 'rm -rf /website2; mkdir /website2' > /dev/null
end=$(date +%s%N | cut -b1-13)
timeTaken=$(bc <<< "scale=2; ($end - $start)/1000")
printf "Time taken : $timeTaken seconds \n\n"

start=$(date +%s%N | cut -b1-13)
echo 'Copying over files...'
sshpass -f <(printf '%s\n' $DOKEY) scp -r archive.zip root@motleytech.net:/website2 > /dev/null
end=$(date +%s%N | cut -b1-13)
timeTaken=$(bc <<< "scale=2; ($end - $start)/1000")
printf "Time taken : $timeTaken seconds \n\n"

start=$(date +%s%N | cut -b1-13)
echo 'Unzipping and updating website...'
sshpass -f <(printf '%s\n' $DOKEY) ssh root@motleytech.net 'cd /website2; unzip -q archive.zip; rm archive.zip; cd /; rm -rf /website; mv /website2 /website' > /dev/null
rm archive.zip
end=$(date +%s%N | cut -b1-13)
timeTaken=$(bc <<< "scale=2; ($end - $start)/1000")
printf "Time taken : $timeTaken seconds \n\n"

totalTime=$(bc <<< "scale=2; ($end - $topTime)/1000")
printf "Total time : $totalTime seconds \n\n"
